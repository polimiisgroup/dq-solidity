pragma solidity ^0.6.1;
pragma experimental ABIEncoderV2;

contract Transport{
    
  constructor() public { owner = msg.sender; }

  address payable owner;
  
  event Processed (uint delta);
    
  struct Measure {
    uint temperature;
    uint time;
  }
  
  struct StoredRecord {
  Measure smeasures;
  bool received;
  }
  
  StoredRecord[] public mem;
  uint public count;

  function getMeasures(uint i) view public returns(Measure memory a){
    return (mem[i].smeasures);
  }
  
//This function receives an array of measures, all the additional parameters needed for DQ assessment and invokes the correspondent functions to assess the single metrics storing the results in memory.
//Before returning it stores the measures in the contract memory
  function evaluateStream(Measure memory value, uint submissionTime) payable public returns (bool ok) {
      mem.push();
      mem[count].smeasures = value;
//note: accuracy and timeliness are assessed per single value, and not in aggregate form
      mem[count].received = true;
      count++;
      emit Processed(block.timestamp - submissionTime);
      return true;
  }
  
//This function is needed in order to cancel the contract from the blockchain whenever it is no longer needed. Only the contract creator address can invoke it.
  function destroy() public {
        if (msg.sender == owner) selfdestruct(owner);
  }
  
  function getTime() view public returns (uint time) {
      return now;
  }
  
  
//The fallback function is needed for the contract to receive Ether
  fallback() external payable {
  }
}