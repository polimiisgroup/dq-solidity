pragma solidity ^0.6.1;
pragma experimental ABIEncoderV2;

contract Transport{
    
  constructor() public { owner = msg.sender; }

  address payable owner;
  
  event Processed (uint delta);
    
  struct Measure {
    uint temperature;
    uint time;
  }
  
  struct StoredRecord {
  Measure[] smeasures;
  bool received;
  }
  
  StoredRecord[] public mem;
  uint public count;


  function getMeasures(uint i) view public returns(Measure[] memory a){
    return (mem[i].smeasures);
  }
  
  function evaluateStream(Measure [] memory values, uint submissionTime) payable public returns (bool ok) {
      mem.push();
      uint t=values.length;
      for (uint8 i=0; i<t;i++)    {
          mem[count].smeasures.push(values[i]);
      }
      mem[count].received = true;
      count++;
      emit Processed(block.timestamp - submissionTime);
      return true;
  }
  
//This function is needed in order to cancel the contract from the blockchain whenever it is no longer needed. Only the contract creator address can invoke it.
  function destroy() public {
        if (msg.sender == owner) selfdestruct(owner);
  }
  
//The fallback function is needed for the contract to receive Ether
  fallback() external payable {
  }
}