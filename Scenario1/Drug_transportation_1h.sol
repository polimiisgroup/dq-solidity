pragma solidity ^0.6.1;
pragma experimental ABIEncoderV2;

contract Transport{
    
  constructor(uint accuracyTemperature, uint accuracyTolerance, uint precisionTolerance, uint precisionSamples, uint timelinessDelta, uint completenessLenght, uint completenessWindow) public { 
    owner = msg.sender; 
    params.accuracyTemperature = accuracyTemperature;
    params.accuracyTolerance = accuracyTolerance;
    params.precisionTolerance = precisionTolerance;
    params.precisionSamples = precisionSamples;
    params.timelinessDelta = timelinessDelta;
    params.completenessWindow = completenessWindow;
    params.completenessLenght = completenessLenght;
  }
  
  address payable owner;
  
  event Processed (uint delta);
    
  struct Measure {
    uint temperature;
    uint time;
  }
  
  struct StoredRecord {
  Measure smeasures;
  bool saccuracy;
  bool scompleteness;
  bool sprecision;
  bool stimeliness;
  }
  
  struct DQParams {
      uint accuracyTemperature; 
      uint accuracyTolerance; 
      uint precisionTolerance; 
      uint precisionSamples; 
      uint timelinessDelta;
      uint completenessLenght;
      uint completenessWindow;
  }
  
  DQParams public params;
  
  
  StoredRecord[] public mem;
  uint public count;

//This function measures the average temperature from a batch of measurements, provided as an array of temperature-timestamp pairs  
  function avgTemp(uint length) view public returns (uint m) {
      m = 0;
      if (length > mem.length)
        length = mem.length;
      for (uint i=mem.length; i>mem.length-length; i--) {
        m = m + mem[i-1].smeasures.temperature;
      }
      m = m / length;
      return m;
  }  
  
  function getAccuracy(uint i) view public returns(bool res){
    return mem[i].saccuracy;
  }
  
    function getCompleteness(uint i) view public returns(bool res){
    return mem[i].scompleteness;
  }
  
    function getPrecision(uint i) view public returns(bool res){
    return mem[i].sprecision;
  }
  
    function getTimeliness(uint i) view public returns(bool res){
    return mem[i].stimeliness;
  }
    function getMeasures(uint i) view public returns(Measure memory a){
    return (mem[i].smeasures);
  }
  
  function updateParameters(uint accuracyTemperature, uint accuracyTolerance, uint precisionTolerance, uint precisionSamples, uint timelinessDelta, uint completenessLenght, uint completenessWindow) payable public {
    params.accuracyTemperature = accuracyTemperature;
    params.accuracyTolerance = accuracyTolerance;
    params.precisionTolerance = precisionTolerance;
    params.precisionSamples = precisionSamples;
    params.timelinessDelta = timelinessDelta;
    params.completenessWindow = completenessWindow;
    params.completenessLenght = completenessLenght;
  }
  
  
//This function receives an array of measures, all the additional parameters needed for DQ assessment and invokes the correspondent functions to assess the single metrics storing the results in memory.
//Before returning it stores the measures in the contract memory
  function evaluateStream(Measure memory value, uint submissionTime) payable public returns (bool ok) {
      mem.push();
      mem[count].smeasures = value;
//note: accuracy and timeliness are assessed per single value, and not in aggregate form
      mem[count].saccuracy = Accuracy(value, params.accuracyTemperature, params.accuracyTolerance);
      mem[count].stimeliness = Timeliness(value, params.timelinessDelta);

//note: Completeness and precision are assessed for the current value, based on the historical values
      mem[count].scompleteness = Completeness(value, params.completenessLenght, params.completenessWindow);
      mem[count].sprecision = Precision(value, params.precisionTolerance, params.precisionSamples);
      count++;
      emit Processed(block.timestamp - submissionTime);
      return true;
  }
  
//This function receives a measure, the reference value, and the accepted tolerance needed to mark the value as non qualitative and then it assesses accuracy of the given value (true/false)
  function Accuracy(Measure memory values, uint mtemperature, uint tolerance) public pure returns (bool check) {
        if (values.temperature>(mtemperature+tolerance) || values.temperature<(mtemperature-tolerance)) 
            return false;
        else 
            return true;
  }

//This function receives a measure, the expected batch lenght, and the length of the time window.
  function Completeness(Measure memory values, uint len, uint window) view public returns (bool complete) {
      uint mem_t = count+1;
      while (mem_t > 0 && mem[count].smeasures.time - mem[mem_t-1].smeasures.time < window) {
          mem_t--;
      }
      
      if (count+1 - mem_t >= len)
        return true;
      return false;
  }  
  
//This function receives a batch of measures and the maximum standard deviation accepted and, after measuring the standard deviation of the received batch it assesses precision by comparing it with the threshold (true/false)
  function Precision(Measure memory values, uint threshold, uint length) view public returns (bool ok) {
      uint m = avgTemp(length);
      uint sqm;
      if (length > mem.length)
        length = mem.length;
      
      if (values.temperature >= m)
          sqm = ((values.temperature-m)*(values.temperature-m)) / length;
        else
          sqm = ((m-values.temperature)*(m-values.temperature)) / length;
      
      if (sqm > (threshold*threshold)) {
          return false;
      }
      return true;
  }  
  
//This function receives a measure, and the maximum delay accepted needed to mark the value as non qualitative and then it assesses timeliness of the given value (true/false) 
  function Timeliness(Measure memory times, uint delta) view public returns (bool ok) {
        if ((now > times.time) && ((now-times.time)>delta)) 
          return false;
        else 
          return true;
  }   

//This function is needed in order to cancel the contract from the blockchain whenever it is no longer needed. Only the contract creator address can invoke it.
  function destroy() public {
        if (msg.sender == owner) selfdestruct(owner);
  }
  
  function getTime() view public returns (uint time) {
      return now;
  }
  
  
//The fallback function is needed for the contract to receive Ether
  fallback() external payable {
  }
}